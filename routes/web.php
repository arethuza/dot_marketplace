<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great! 
|
*/
Route::middleware('auth')->group(function() {
	//Product Routes
	Route::get('/', function () {
		return 'ini dashboard';
	});

	Route::get('/kategori', 'KategoriController@index');

	Route::get('/aku', function(){
		return "Halo aku!";
	});

	Route::get('/halo/{namaq}', function($namaq){
		return "Halo $namaq";
	});

	Route::get('/halo/{namaq}/{nama}', function($namaq, $asmane){
		return "Halo, $namaq $asmane !";
	});

	Route::get('/users', 'ControllerKu@ads');

	Route::get('/adds/{nana}', 'ControllerKu@add');

	Route::post('/kategori', 'KategoriController@index');

	Route::post('/kategori/store', 'KategoriController@store');

	Route::get('/kategori/delete/{id_kategori}', 'KategoriController@delete');

	Route::get('/kategori/detail/{id_kategori}', 'KategoriController@detail');

	Route::get('/kategori/edit/{id_kategori}', 'KategoriController@edit');

	Route::post('/edit', 'KategoriController@edit');

// route product
	Route::get('/product', 'ProductsController@index');

	Route::get('/product/{id}/detail', 'ProductsController@detail');

	Route::get('/product/{id}/edit', 'ProductsController@edit');

	Route::post('/product/store', 'ProductsController@store');

	Route::get('/product/{id}/update', 'ProductsController@update');

	Route::get('/product/delete/{id}', 'ProductsController@delete');
});
//LOGIN

Route::get('/login', 'LoginController@showLogin')->name('login'); // buat show login form
Route::post('/login', 'LoginController@doLogin')->name('login.doLogin');

//LOGOUT

Route::get('/logout', 'LoginController@doLogout');

//CUSTOMERS
Route::get('/customers', 'CustomersController@index');

Route::get('/customers/{id_customers}/detail', 'CustomersController@detail');

Route::get('/customers/{id_customers}/edit', 'CustomersController@edit');

Route::post('/customers/store', 'CustomersController@store');

Route::get('/customers/{id_customers}/update', 'CustomersController@update');

Route::get('/customers/delete/{id_customers}', 'CustomersController@delete');

Route::prefix('orders')->group(function(){
	Route::get("/", "OrderController@index");
	Route::post("/", "OrderController@store");
	Route::get('/detail/{id}', 'OrderController@detail');
	Route::post('/detail/{id}/add', 'OrderController@addProduct')->name('order.add');
	Route::get('/{id}/delete', 'OrderController@delete');
});