<?php

use Illuminate\Http\Request;
use App\Products;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Membuat Routing API dengan prefix "localhost/api/v1/{badlaldasd}"

Route::prefix('v1')->group(function(){
	Route::prefix('belajar')->group(function(){
		Route::get('cobaget', 'API\V1\CobaController@method_get');
		Route::post('cobapost', 'API\V1\CobaController@method_post');
	});

	Route::prefix('kategori')->group(function(){
		Route::get('/', 'API\V1\KategoriController@index');
		Route::get('/{id}', 'API\V1\KategoriController@detail');
		Route::post('/store', 'API\V1\KategoriController@store');
	});
});

Route::apiResource('category', 'API\V1\CategoryController');
Route::apiResource('customers', 'API\V1\KustomersController');
Route::apiResource('products', 'API\V1\ProductsController');
// Route::get('/produk/{id}', function(Products $id) {
//     return apiResponseBuilder(200,new SongResource($id));
// });

// Route::get('/produk', function() {
//     return apiResponseBuilder(200,SongResource::collection(Products::all()));
// });

// AUTH Ajax
// Register 
Route::post('/register', 'API\V1\Auth\RegisterController@register');

// Login 
Route::post('/login', 'API\V1\Auth\LoginController@login');

// Detail
Route::group(['middleware' => 'auth:api'], function(){
	Route::get('/detail', 'API\V1\UserController@detail');
});