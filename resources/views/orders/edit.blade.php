  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customers
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Customers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <td>Id</td>
                  <td>:</td>
                  <td>{{ $dataCustomers->id }}</td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td>:</td>
                  <td>{{ $dataCustomers->name }}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td>{{ $dataCustomers->email }}</td>
                </tr>
                <tr>
                  <td>Password</td>
                  <td>:</td>
                  <td>{{ $dataCustomers->password }}</td>
                </tr>
                <tr>
                  <td>No. Telp</td>
                  <td>:</td>
                  <td>{{ $dataCustomers->phone_number }}</td>
                </tr>
                <tr>
                  <td>Alamat</td>
                  <td>:</td>
                  <td>{{ $dataCustomers->address }}</td>
                </tr>
              </table>

              <a class="btn btn-warning" href="/customers">Back</a>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')