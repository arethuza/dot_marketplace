@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detail Order
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h4><strong>Tambah Data Barang</strong></h4>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('order.add', $detailId) }}" method="post" enctype="multipart/form-data">

              @if($errors->has('nama'))
                <div style="padding-left:10px">
                  <h5><strong><font color="red">{{ $errors->first('nama')}}</font></strong></h5>
                </div>
              @endif
              <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Pilih Product :</label>
                  <select type="text" class="form-control" name="id_products">
                      <option></option>
                    @foreach ($product as $data)
                      <option value="{{ $data->id }}">{{ $data->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Jumlah :  </label>
                  <input name="quantity" class="form-control" type="input">
                </div>
              </div>

                {{ csrf_field() }}
                <div class="box-footer" style="margin-left:16px;">
                  <button type="submit" class="btn btn-primary">Tambah Data Product</button>
                </div><br />
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Tabel Data Customer</strong></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama Product</th>
                  <th>Harga satuan</th>
                  <th>Qty</th>
                  <th>Harga Total</th>
                </tr>
                @foreach($orderDetail as $data)
                <tr>
                  <td>{{ $counter++ }}</td>
                  <td>{{ $data->product->name }}</td>
                  <td>{{ number_format($data->product->unit_price) }}</td>
                  <td>{{ $data->quantity }}</td>
                  <td>Rp{{ number_format($data->total,2,',','.') }}</td>
                </tr>

              @endforeach
                <tr>
                  <td colspan="4" class="text-center">Total</td>
                  <td>Rp{!! number_format($total->total) !!}</td>
                </tr>


              </table>
            </div>
          </div>
      </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('base.footer')