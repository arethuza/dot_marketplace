
@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Contoh Form dan Tabel
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="{{url('/product/store')}}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1">Nama Produk</label>
								<input type="text" name="name" class="form-control" placeholder="Masukkan nama produk">
								<label for="exampleInputEmail1">Nama Kategori</label>
								<select class="form-control" name="id_kategori">
									<option></option>
									@foreach ($category as $ktg)
									<option value="{{ $ktg->id_kategori }}">{{ $ktg->nama_kategori }}</option>
									@endforeach
								</select>
								<label for="exampleInputEmail1">Harga Produk</label>
								<input type="text" name="unit_price" class="form-control" placeholder="Masukkan nama harga produk">
								<label for="exampleInputEmail1">Foto Produk</label>
								<input type="file" name="photo">
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Tabel</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>Nama Product</th>
								<th>Harga</th>
								<th>Kategori</th>
								<th>Action</th>
							</tr>
							<?php $no = 1; ?>
							@foreach($dataProduct as $item)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->unit_price }}</td>
								<td>{{ $item->category->nama_kategori }}</td>
								<td><img src="/photo/product/{{ $item->photo }}" style="width: 32px; height: 32px;"></td>
								<td>
									<a href="{{url('/product/'.$item->id.'/detail')}}" class="btn btn-primary">Info</a>
									<a href="{{url('/product/'.$item->id.'/edit')}}" class="btn btn-info">Edit</a>
									<a href="{{url('/product/delete/'.$item->id)}}" class="btn btn-danger">Delete</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

@include('base.footer')