@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Form Product
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="/kategori/edit/{{ $dataKategori->id_kategori }}" method="post">
                <table class="table table-bordered">
                  <tr>
                    <td>Nama Kategori</td>
                    <td>:</td>
                    <td><input type="text" name="nama" value="{{ $dataKategori->nama_kategori }}" class="form-control" ></td>
                  </tr>

                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="PUT">

                </table>

                  <a href="/kategori" type="submit" class="btn btn-primary">Kembali</a> &nbsp;&nbsp;
                  <button type="submit" class="btn btn-warning">Edit Data Produk</button>
              </form>
            </div>
          </div>
      </div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	@include('base.footer')
