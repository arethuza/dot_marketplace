
@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Contoh Form dan Tabel
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Tabel</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>Nama</td>
								<td>Kategori</td>
								<td>Foto Produk</td>
								<td>Harga</td>
							</tr>
							<tr>
								<td>{{ $dataProduct->id }}</td>
								<td>{{ $dataProduct->name }}</td>
								<td>{{ $dataProduct->category->nama_kategori }}</td>
								<td><img src="/photo/product/{{ $dataProduct->photo }}" style="width: 100px; height: 100px;"></td>
								<td>{{ $dataProduct->unit_price }}</td>
							</tr>
						</table>
						<a href="/product" class="btn btn-primary">Back</a>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

@include('base.footer')