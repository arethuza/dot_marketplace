@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Form Product
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Input Product</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="/kategori/store" method="post">

						{{ csrf_field() }}

						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1">Product Name</label>
								<input type="text" name="nama_kategori" class="form-control" placeholder="Masukkan nama produk">
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Tabel</h3>
						<form action="/product" method="GET">
							<span class="pull-right">
								<input type="text" name="search" class="form-control" placeholder="Search here ..">
							</span>
						</form>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th>Id</th>
								<th>Kategori</th>
								<th>Jumlah Produk</th>
								<!-- <th>Status</th> -->
								<th>Aksi</th>
							</tr>
							@foreach($kategori as $item)
							<tr>
								<td>{{ $item->id_kategori }}</td>
								<td>{{ $item->nama_kategori }}</td>
								<td>{{ $item->product_count }}</td>
								<!-- <td>
									@if($item->trashed())
									<span class="label label-danger">Not Available</span>
									@else
									<span class="label label-success">Available</span>
									@endif
								</td> -->
								<td>
									<a href="/kategori/detail/{{ $item->id_kategori }}" class="btn btn-primary btn-sm"><i class="fa fa-list">&nbsp;</i>Detail</a>
									<a href="/kategori/edit/{{ $item->id_kategori }}" class="btn btn-info btn-sm"><i class="fa fa-pencil">&nbsp;</i>Edit</a>
									<a href="/kategori/delete/{{ $item->id_kategori }}" class="btn btn-danger btn-sm"><i class="fa fa-trash">&nbsp;</i>Delete</a>
								</td>
								<td></td>
							</tr>                  
							@endforeach
						</table>
					</div>

					<div class="text-center">
						{!! $kategori->appends(request()->all())->links() !!}
					</div>

				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	@include('base.footer')
