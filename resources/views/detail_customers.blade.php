  
@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Contoh Form dan Tabel
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary" style="height: 357px;">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="/photo/customer/1556535727.jpg" style="width: 128px; height: 128px;" alt="User profile picture">

              <h3 class="profile-username text-center">{{ $data_cust->first_name }} {{ $data_cust->last_name }}</h3>

              <p class="text-muted text-center">{{ $data_cust->email }}</p>

              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      </div>
<div class="col-md-9">
          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Nomor Ponsel</strong>

              <p class="text-muted">
                {{ $data_cust->phone }}
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>

              <p class="text-muted">{{ $data_cust->address }}</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Password</strong>

              <p>{{ $data_cust->password }}</p>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
          <!-- /.box -->
        </div></section></div>
			

@include('base.footer')