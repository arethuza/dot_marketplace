
@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Contoh Form dan Tabel
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Tabel</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>Nama</td>
								<td>Jumlah Produk</td>
								<td>Keterangan</td>
							</tr>
							<tr>
								@if($dataKategori->trashed())
								<td>{{ $dataKategori->id_kategori }}</td>
								<td>{{ $dataKategori->nama_kategori }}</td>
								<td>{{ $dataKategori->product_count }}</td>
								<td>Data Ini Sudah Dihapus</td>
								@else
								<td>{{ $dataKategori->id_kategori }}</td>
								<td>{{ $dataKategori->nama_kategori }}</td>
								<td>{{ $dataKategori->product_count }}</td>
								<td>Data Ini Tersedia</td>
								@endif
							</tr>
						</table>
						<a href="/kategori" class="btn btn-primary">Back</a>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

@include('base.footer')