
@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Contoh Form dan Tabel
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="{{url('/customers/store')}}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1">Email</label>
								<input type="email" name="email" class="form-control" placeholder="Masukkan Email" required>
								<label for="exampleInputEmail1">First Name</label>
								<input type="text" name="first_name" class="form-control" placeholder="Masukkan Nama Depan" required>
								<label for="exampleInputEmail1">Last Name</label>
								<input type="text" name="last_name" class="form-control" placeholder="Masukkan Nama Belakang" required>
								<label for="exampleInputEmail1">Address</label>
								<textarea name="address" class="form-control" placeholder="Masukkan Alamat"></textarea required>
								<label for="exampleInputEmail1">Phone</label>
								<input type="number" name="phone" class="form-control" placeholder="Masukkan Nomor Ponsel" required>
								<label for="exampleInputEmail1">Password</label>
								<input type="text" name="password" class="form-control" placeholder="Masukkan Password" required>
								<!-- <label for="exampleInputEmail1">Photo</label>
								<input type="file" name="photo"> -->
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Tabel</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>Email Customers</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Address</th>
								<th>Phone</th>
								<th>Password</th>
								<!-- <th>Photo</th> -->
								<th>Action</th>
							</tr>
							<?php $no = 1; ?>
							@foreach($data_cust as $item)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $item->email }}</td>
								<td>{{ $item->first_name }}</td>
								<td>{{ $item->last_name }}</td>
								<td>{{ $item->address }}</td>
								<td>{{ $item->phone }}</td>
								<td>{{ $item->password }}</td>
								<!-- <td><img src="/photo/customer/{{ $item->photo }}" style="width: 32px; height: 32px;"></td> -->
								<td>
									<a href="{{url('/customers/'.$item->id_customers.'/detail')}}" class="btn btn-primary">Info</a>
									<a href="{{url('/customers/'.$item->id_customers.'/edit')}}" class="btn btn-info">Edit</a>
									<a href="{{url('/customers/delete/'.$item->id_customers)}}" class="btn btn-danger">Delete</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

@include('base.footer')