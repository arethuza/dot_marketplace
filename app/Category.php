<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;

	protected $table = "kategori";

	protected $primaryKey = "id_kategori";

	protected $fillable = ["id_kategori", "nama_kategori", "product_count"];
}
