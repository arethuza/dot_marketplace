<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';

    protected $primaryKey = "id_customers";

    protected $fillable = ["email", "first_name", "last_name", "address", "phone", "password"/*, "photo"*/];

    /*public function category() {
    	return $this->hasOne(Category::class, "id_kategori", "id_kategori")->withTrashed();
    }*/
}
