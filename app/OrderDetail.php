<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
  protected $table = 'order_details';

  protected $fillable = ['id_orders','id_products','quantity','total'];

  function order(){
    return $this->hasOne(Orders::class, "id_orders", "id_orders");
  }

  function product(){
    return $this->hasOne(Products::class, "id", "id_products");
  }
}
