<?php

namespace App;
use App\Customers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
  protected $table = 'orders';
  protected $fillable = ['id_customers','total'];

    public function customer() {
    	return $this->hasOne(Customers::class, "id_customers", "id_customers");
    }

}
