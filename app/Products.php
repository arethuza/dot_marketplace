<?php

namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
	use SoftDeletes;
	
    protected $primaryKey = "id";

    protected $table = 'products';

    protected $fillable = ["id","name", "id_kategori", "unit_price", "photo"];

    public function category() {
    	return $this->hasOne(Category::class, "id_kategori", "id_kategori")->withTrashed();
    }

}