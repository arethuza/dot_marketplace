<?php

/**
 * response api validation errors
 *
 * @param null $message
 * @param null $errors
 * @param int $status_code
 * @return \Illuminate\Http\JsonResponse
 */
function apiResponseValidationFails($message = null, $errors = null, $status_code = 422) {
    return response()->json([
        'message' => $message,
        'status_code' => $status_code,
        'data' => [
            'errors' => $errors
        ]
    ], $status_code);
}

/**
 * api response success
 *
 * @param null $message
 * @param null $data
 * @param int $status_code
 * @return \Illuminate\Http\JsonResponse
 */
function apiResponseSuccess($message = null, $data = null, $status_code = 200) {
    return response()->json([
        'message' => $message,
        'status_code' => $status_code,
        'data' => $data
    ], $status_code);
}

/**
 * api response errors
 *
 * @param null $message
 * @param null $data
 * @param int $status_code
 * @return \Illuminate\Http\JsonResponse
 */
function apiResponseErrors($message = null, $data = null, $status_code = 401) {
    return response()->json([
        'message' => $message,
        'status_code' => $status_code,
        'data' => $data
    ], $status_code);
}