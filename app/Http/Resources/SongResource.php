<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SongResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'unit_price' => $this->unit_price,
            'photo' => 'localhost:8000/'.$this->photo,
            'kategori' => [
                'id_kategori' => $this->id_kategori,
                'nama_kategori' => $this->category->nama_kategori
            ]
        ];
    }
}
