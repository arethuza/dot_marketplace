<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\Category;
use Session;
use Validator;
use Exception;
use SoftDeletes;

class ProductsController extends Controller
{
	public function index()
	{
		$category = Category::all();
		$dataProduct = Products::query(); // Membersihkan query kosong di tabel produk
		//searching data
		//kalo search query ada dan panjangnya lebih dari 1
		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$dataProduct->where(
				"name", "like", "%" . request()->query("search") . "%"
			);
		}


		//QUERY PAGINATION
		$pagination = 5;
		$dataProduct = $dataProduct->latest()->paginate(5);
		
		// Handle perpindahannya page
		$number = 1; // Default page

		if (request()->has('page') && request()->get('page') > 1) 
		{
			$number += (request()->get('page') - 1) * $pagination;
		}

		// Select id, nama, diurutkan dari nama
		$categories = Category::select(["id_kategori", "nama_kategori"])->orderBy("nama_kategori")->get();

		return view('product', compact('dataProduct', 'number', 'category', 'categories'));
	}

	public function detail($id)
	{
		$dataProduct = Products::where('id', $id)->first();

		return view('detail', compact('dataProduct'));
	}

	public function edit($id)
	{
		$dataProduct = Products::find($id);

		$dataProduct->name = $request->name;
		$dataProduct->category = $request->category;
		$dataProduct->unit_price = $request->unit_price;

		$dataProduct->save();

		return redirect()->back();
	}

	public function store(Request $request)
	{
		//Validator::make($request->all(), /*Rulesnya -> */['name' => 'required', 'category' => 'required', 'unit_price' => 'required'])->validate();

		/*$dataProduct = new Products;

		$dataProduct->name = $request->name;
		$dataProduct->category = $request->category;
		$dataProduct->unit_price = $request->unit_price;

		$dataProduct->save();

		if ($dataProduct) {
			$request->session()->flash('success', 'Berhasil menambahkan data!');
		}

		return redirect()->back();*/
		
		try{
			$this->validate($request,[
				'nama_kategori' => 'required',     

			]);
            \DB::beginTransaction();

            $imgName = time().'.'.request()->photo->getClientOriginalExtension();

            request()->photo->move(public_path('photo\product'), $imgName);
            //QUERY STORE
            $product = new Products();
            $product->name = $request->name;
            $product->id_kategori = $request->id_kategori;
            $product->unit_price = $request->unit_price;
            $product->photo = $imgName;
            $product->save();

            //UPDATE PRODUCT COUNT
            Category::find($request->id_kategori)->increment('product_count', 1);
            \DB::commit();

            Session::flash('success', 'Berhasil menambahkan data!');
            return redirect()->back();
        }catch (Exception $e){
            \DB::rollBack();
            Session::flash('success', 'Gagal menambahkan data!');
            return redirect()->back()->with($e);
        }

	}

	public function update(Request $request)
	{
		$dataProduct = Products::find($id);

		$dataProduct->name = $request->name;
		$dataProduct->id_kategori =$request->id_kategori;
		$dataProduct->unit_price = $request->unit_price;

		$dataProduct->save();

		if ($product)
        {
            Session::flash('success','Berhasil mengubah data');
        }

		return redirect()->back();
	}

	public function delete($id)
	{
		$dataProduct = Products::find($id);
		dd($dataProduct);

		Category::find($id)->decrement('product_count', 1);

		$dataProduct->delete();

		return redirect()->back();
	}
}
