<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Customers;
use App\Products;
use App\Orders;
use App\OrderDetail;
use Session;

class OrderController extends Controller
{

	public function index(){
		$category = Category::all();
		$product = Products::all();
		$order = Orders::all();
		$orderDetail = OrderDetail::all();
		$customer = Customers::all();
		$counter = 1;
		return view('orders/index', compact('product','order','customer','category','orderDetail','counter')); }

		public function store(Request $request){
			Orders::create([
				'id_customers'=>$request->customer_id,
				'total'=>0
			]);
			Session::flash('sukses', 'Sukses Menambahkan Pengguna');
			return redirect()->back();
		}

		public function detail($id){
			$dataOrderDetail = OrderDetail::where('id_orders', $id)->get();
			$detailId = $id;
			$total = Orders::select('total')->where('id_orders', $id)->first();


			$category = Category::all();
			$product = Products::all();
			$order = Orders::all();
			$orderDetail = OrderDetail::all();
			$customer = Customers::all();
			$counter = 1;
			return view('orders/detail',compact('product','order','total','customer','category','detailId','orderDetail','counter'));
		}

		public function tambahOrderDetail(Request $request){
			OrderDetail::create($request->all());
			Session::flash('sukses', 'Sukses Menambahkan Order');
			return redirect()->back();
		}


		public function addProduct(Request $request, $id)
		{
			// get data Order detail by product id
			$checkProduct = OrderDetail::where('id_products', $request->id_products)->where('id_orders', $id)->first();
			// mengambil data satu baris pada products dengan mengambil id pada input
			$dataProduct = Products::where('id', $request->id_products)->first();

			// check jika product sudah dimasukkan
			if ($checkProduct) {
			// menambah jumlah yang sebelumnya dengan yang baru
				OrderDetail::where('id_orders',$id)->where('id_products',$request->id_products)->update([
					'quantity' => $checkProduct->quantity + $request->quantity,
					'total' => $checkProduct->total + ($request->quantity * $dataProduct->unit_price)
				]);               
			} else {
			// menambah poduct baru pada order
				$dataOrderDetail = new OrderDetail;
				$dataOrderDetail->id_orders     = $id;
				$dataOrderDetail->id_products   = $request->id_products;
				$dataOrderDetail->quantity      = $request->quantity;
				$dataOrderDetail->total         = $request->quantity * $dataProduct->unit_price;
				$dataOrderDetail->save();
			}

			// mengambil total pada tabel order
			$getOrder = OrderDetail::where('id_orders', $id)->get();

			// menjumlah semua total pada order
			$total = $getOrder->sum('total');

			// insert data total pada tabel order
			Orders::where('id_orders',$id)->update([
				'total'=>$total
			]);          

			return redirect()->back();
		}

		public function delete($id)
		{
			OrderDetail::where('id_orders',$id)->delete();
			Orders::where('id_orders',$id)->delete();

			return redirect()->back();

		}

	}
