<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;

class LoginController extends Controller
{
	// Show login formnya

    public function showLogin()
    {
            
        return view('auth.login');
    }

    // Proses login

    public function doLogin(Request $request)
    {
    	// Get User Credentials (Email & Password)
    	$credential = $request->except("_token");
        $email = $request->email;
    	// Attempt buat login
    	if (Auth::attempt($credential)) {
            $data = User::where('email', $email)->first();
            Session::put('name', $data->name);
            Session::put('logged_in', TRUE);
            return redirect('/kategori');
    	} else {
    		return redirect()->back();
    	}
    }

    public function doLogout()
    {
    	Auth::logout();
    	return redirect('login');
    }
}
