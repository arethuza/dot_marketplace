<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Session;
use SoftDeletes;

class KategoriController extends Controller {

	public function index()
	{
		$kategori = Category::query();

		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$kategori->where(
				"name", "like", "%" . request()->query("search") . "%"
			);
		}

		// Query paginationnya
		$pagination = 5;
		$kategori = $kategori->withTrashed()->latest()->paginate(5);

		// Handle perpindahannya page
		$number = 1; // Default page

		// kalo pagenya > 1, (number(1) += page - 1) * pagination(5)
		// 1 += (page(misal = 2) - 1) * 5
		// 1 += (2-1) * 5
		// 1 += 1*5 == 6
		if (request()->has('page') && request()->get('page') > 1) {
			$number += (request()->get('page') - 1) * $pagination;
		}
		return view('template', compact('kategori', 'number'));
	}

	public function store(Request $request)
    {
        try{
            \DB::beginTransaction();

            //QUERY STORE
            $dataKategori = new Category();
            $dataKategori->product_count = "0";
            $dataKategori->nama_kategori = $request->nama_kategori;
            $dataKategori->save();
            \DB::commit();

            Session::flash('success', 'Berhasil menambahkan data!');
            return redirect()->back();
        }catch (Exception $e){
            \DB::rollBack();
            Session::flash('success', 'Gagal menambahkan data!');
            return redirect()->back()->with($e);
        }
    }

    public function detail($id)
	{
		$dataKategori = Category::where('id_kategori', $id)->withTrashed()->first();

		return view('detail_kategori', compact('dataKategori'));
	}

	public function delete($id_kategori)
	{
		$dataKategori = Category::find($id_kategori);
		$dataKategori->delete();

		return redirect()->back();
	}

	public function edit($id)
	{
		$dataKategori = Category::find($id);

		$dataKategori->nama_kategori = $request->nama_kategori;

		$dataKategori->save();

		return redirect()->back();
	}


}

/* End of file KategoriController.php */
/* Location: .//E/xampp/htdocs/dota_cademy/dota_marketplace/app/Http/Controllers/KategoriController.php */