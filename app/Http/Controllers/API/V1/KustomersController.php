<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class KustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = Customers::all();
            $code = 200;
            $response = $data; 

        } catch (Exception $e) {

            $code = 500;
            $response = $e->getMessage(); 
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                'email' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'password' => 'required',

            ]);

            $data_cust = new Customers;
            if (!$data_cust) throw new \Exception("Error Processing Request", 1);
            if (!isset($request->email)) throw new \Exception("data harus isi", 1);

            $data_cust->email = $request->email;
            $data_cust->first_name = $request->first_name;
            $data_cust->last_name = $request->last_name;
            $data_cust->address = $request->address;
            $data_cust->phone = $request->phone;
            $data_cust->password = $request->password;
            $data_cust->photo = '1556535727.jpg';
            $data_cust->save();

            $code = 200;
            $response = $data_cust; 

        } catch (Exception $e) {
    // $code = 500;
    // $response = $e->getMessage(); 

            if ($e instanceof ValidationException) {
                $code = 400;
                $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }

        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = Customers::findOrFail($id);
            $code = 200;
            $response = $data;

        } catch (Exception $e) {
    // $code = 500;
    // $response = $e->getMessage(); 
            if ($e instanceof ModelNotFoundException) {
                $code = 404;
                $response = 'not found data';
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
                'email' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'password' => 'required',   

            ]);
            $data_cust = Customers::find($id);

            if (!$data_cust) throw new \Exception("Error Processing Request", 1);
            if (!isset($request->email)) throw new \Exception("data harus isi", 1);
            
            $data_cust->email = $request->email;
            $data_cust->first_name = $request->first_name;
            $data_cust->last_name = $request->last_name;
            $data_cust->address = $request->address;
            $data_cust->phone = $request->phone;
            $data_cust->password = $request->password;
            $data_cust->photo = '1556535727.jpg';
            $data_cust->save();

            $code = 200;
            $response = $data_cust;

        } catch (Exception $e) {
    // $code = 500;
    // $response = $e->getMessage();
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            } 
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Customers::find($id);
            $data->delete();
            $code = 200;
            $response = $data;

        } catch (Exception $e) {
            $code = 500;
            $response = $e->getMessage(); 
        }
        return apiResponseBuilder($code,$response);
    }
}
