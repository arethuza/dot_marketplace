<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function detail()
    {
    	$user = Auth::user();
    	return apiResponseSuccess('Detail User Login', $user, 200);
    }
}
