<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index()
	{
		try {
			$data = Category::all();
			$code = 200;
			$response = $data; 

		} catch (Exception $e) {

			$code = 500;
			$response = $e->getMessage(); 
		}

		return apiResponseBuilder($code, $response);
	}

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request)
	{
		try {
			$this->validate($request,[
				'nama_kategori' => 'required',     

			]);

			$data = new Category;
			if (!$data) throw new \Exception("Error Processing Request", 1);
			if (!isset($request->nama_kategori)) throw new \Exception("data harus isi", 1);

			$data->nama_kategori = $request->nama_kategori;
			$data->product_count = 0;
			$data->save();

			$code = 200;
			$response = $data; 

		} catch (Exception $e) {
	// $code = 500;
	// $response = $e->getMessage(); 

			if ($e instanceof ValidationException) {
				$code = 400;
				$response = $e->errors();
			}else{
				$code = 500;
				$response = $e->getMessage(); 
			}

		}
		return apiResponseBuilder($code,$response);
	}

	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function show($id)
	{
		try {
			$data = Category::findOrFail($id);
			$code = 200;
			$response = $data;

		} catch (Exception $e) {
	// $code = 500;
	// $response = $e->getMessage(); 
			if ($e instanceof ModelNotFoundException) {
				$code = 404;
				$response = 'not found data';
			}else{
				$code = 500;
				$response = $e->getMessage(); 
			}
		}

		return apiResponseBuilder($code,$response);
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function update(Request $request, $id)
	{
		try {
			$this->validate($request,[
				'nama_kategori'          => 'required',     

			]);
			$data = Category::find($id);

			if (!$data) throw new \Exception("Error Processing Request", 1);
			if (!isset($request->nama_kategori)) throw new \Exception("data harus isi", 1);
			$data->nama_kategori = $request->nama_kategori;
			$data->save();

			$code = 200;
			$response = $data;

		} catch (Exception $e) {
	// $code = 500;
	// $response = $e->getMessage();
			if ($e instanceof ValidationException) {
				$code = 400;
				$response = $e->errors();
			}else{
				$code = 500;
				$response = $e->getMessage(); 
			} 
		}
		return apiResponseBuilder($code,$response);
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy($id)
	{
		try {
			$data = Category::find($id);
			$data->delete();
			$code = 200;
			$response = $data;

		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage(); 
		}
		return apiResponseBuilder($code,$response);
	}
}
