<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SongResource;
use App\Products;
use App\Category;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $response = SongResource::collection(Products::all());
            $code = 200;

        } catch (Exception $e) {

            $code = 500;
            $response = $e->getMessage(); 
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'name'           => 'required',
                'id_kategori'    => 'required',
                'unit_price'     => 'required',
                /*'photo'           => 'required', 'image|mimes:jpeg,png,jpg,gif,svg|max:20480'*/
            ]);

            /*$imgName = time().'.'.request()->photo->getClientOriginalExtension();*/
            /*request()->photo->move(public_path('photo\product'), $imgName);*/

            //QUERY STORE
            $product = new Products();
            if (!$product) throw new \Exception("Error Processing Request", 1);
            if (!isset($request->name)) throw new \Exception("data harus isi", 1);

            $product->name = $request->name;
            $product->id_kategori = $request->id_kategori;
            $product->unit_price = $request->unit_price;
            $product->photo = '1556535727.jpg';
            $product->save();

            //UPDATE PRODUCT COUNT
            Category::find($request->id_kategori)->increment('product_count', 1);
            \DB::commit();

            $code = 200;
            $response = $product; 

        }catch (Exception $e){
            if ($e instanceof ValidationException) {
                \DB::rollBack();
                $code = 400;
                $response = $e->errors();
            }else{
                \DB::rollBack();
                $code = 500;
                $response = $e->getMessage(); 
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $response = new SongResource(Products::findOrFail($id));
            $code = 200;

        } catch (Exception $e) {
    // $code = 500;
    // $response = $e->getMessage(); 
            if ($e instanceof ModelNotFoundException) {
                $code = 404;
                $response = 'not found data';
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
