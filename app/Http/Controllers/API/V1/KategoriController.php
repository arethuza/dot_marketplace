<?php
namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Session;
use SoftDeletes;
use Exception;

class KategoriController extends Controller {

	public function index()
	{
		$kategori = Category::query();

		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$kategori->where(
				"name", "like", "%" . request()->query("search") . "%"
			);
		}

		// Query paginationnya
		$pagination = 5;
		$kategori = $kategori->withTrashed()->latest()->paginate(5);

		// Handle perpindahannya page
		$number = 1; // Default page

		// kalo pagenya > 1, (number(1) += page - 1) * pagination(5)
		// 1 += (page(misal = 2) - 1) * 5
		// 1 += (2-1) * 5
		// 1 += 1*5 == 6
		if (request()->has('page') && request()->get('page') > 1) {
			$number += (request()->get('page') - 1) * $pagination;
		}
		return view('template', compact('kategori', 'number'));
	}

	public function store(Request $request)
	{
		try{
			$Category = new Category;

			$Category->nama_kategori = $request->nama_kategori;
			$Category->product_count=0;

			$Category->save();
			$response=$Category;
			$code=200;

			return apiResponseBuilder(200,$Category);
		}catch(Exception $e){
			$response = $e->getMessage();    
			$code = 500;
			return apiResponseBuilder(500,$e->getMessage());
		}
	}

	public function detail($id)
	{
		$dataKategori = Category::where('id_kategori', $id)->withTrashed()->first();

		return view('detail_kategori', compact('dataKategori'));
	}

	public function delete($id_kategori)
	{
		$dataKategori = Category::find($id_kategori);
		$dataKategori->delete();

		return redirect()->back();
	}

}

/* End of file KategoriController.php */
/* Location: .//E/xampp/htdocs/dota_cademy/dota_marketplace/app/Http/Controllers/KategoriController.php */