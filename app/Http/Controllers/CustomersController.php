<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Customers;
use Session;
use Validator;
use Exception;

class CustomersController extends Controller
{
    public function index()
	{
		$data_cust = Customers::query(); // Membersihkan query kosong di tabel produk

		//searching data
		//kalo search query ada dan panjangnya lebih dari 1
		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$data_cust->where(
				"name", "like", "%" . request()->query("search") . "%"
			);
		}

		//QUERY PAGINATION
		$pagination = 5;
		$data_cust = $data_cust->latest()->paginate(5);
		
		// Handle perpindahannya page
		$number = 1; // Default page

		if (request()->has('page') && request()->get('page') > 1) 
		{
			$number += (request()->get('page') - 1) * $pagination;
		}

		return view('customer', compact('data_cust', 'number'));
	}

	public function detail($id_customers)
	{
		$data_cust = Customers::find($id_customers);

		return view('detail_customers', compact('data_cust'));
	}

	public function edit($id_customers)
	{
		$data_cust = Customers::where('id_customers', $id_customers)->first();
		// default laravel itu cari customers.id, nah kalo namanya id_customers, jadinya pake where(), bukan find()
		return view('customers.edit', compact('data_cust'));
	}

	public function store(Request $request)
	{

		$this->validate($request, [
			'email'			=> 'required',
			'first_name'	=> 'required',
			'last_name'		=> 'required',
			'address'		=> 'required',
			'phone'			=> 'required',
			'password'		=> 'required',
			/*'photo'			=> 'required', 'image|mimes:jpeg,png,jpg,gif,svg|max:20480'*/
		]);
		/*$imgName = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('photo\customer'), $imgName);*/

		$data_cust = new Customers;

		$data_cust->email = $request->email;
		$data_cust->first_name = $request->first_name;
		$data_cust->last_name = $request->last_name;
		$data_cust->address = $request->address;
		$data_cust->phone = $request->phone;
		$data_cust->password = $request->password;
		$data_cust->photo = '1556535727.jpg';
		$data_cust->save();

		if ($data_cust) {
			$request->session()->flash('success', 'Berhasil menambahkan data!');
		}

		return redirect()->back();

		/*Validator::make($request->all(), /*Rulesnya -> ['email' => 'required', 'first_name' => 'required', 'last_name' => 'required', 'address' => 'required', 'phone' => 'required', 'password' => 'required'])->validate();

		$imgName = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('photo\customer'), $imgName);

		$data_cust = new Customers;

		$data_cust->email = $request->email;
		$data_cust->first_name = $request->first_name;
		$data_cust->last_name = $request->last_name;
		$data_cust->address = $request->address;
		$data_cust->phone = $request->phone;
		$data_cust->password = $request->password;
		$data_cust->photo = $imgName;

		$data_cust->save();

		if ($data_cust) {
			$request->session()->flash('success', 'Berhasil menambahkan data!');
		}

		return redirect()->back();

		/*try {
			DB::beginTransaction();

			Category::where('id_kategori', $request->category)->increment('product_count');

			$product = new Customers;

			$product->name = $request->name;

			$product->unit_price = $request->unit_price;

			$save = $product->save();
			DB::commit();
			Session::flash('message', 'Berhasil menambahkan!');
			return redirect()->back();
		} catch (Exception $e) {
			dd($product);
			DB::rollback();
			report($e/*ini exceptionnya, kalo $e berarti ya $e);
			Session::flash('message', 'Gagal tambah');
			return redirect()->back();
		}*/
	}

	public function update(Request $request)
	{
		$data_cust = Customers::find($id_customers);

		$data_cust->email = $request->email;
		$data_cust->first_name = $request->first_name;
		$data_cust->last_name = $request->last_name;
		$data_cust->address = $request->address;
		$data_cust->phone = $request->phone;
		$data_cust->password = $request->password;
		$data_cust->photo = $request->photo;

		$data_cust->save();

		return redirect()->back();
	}

	public function delete($id_customers)
	{
		$data_cust = Customers::find($id_customers);

		$data_cust->delete();

		return redirect()->back();
	}
}
