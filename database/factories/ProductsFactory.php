<?php

use App\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
        'name'			=> $faker->word,
        'unit_price'	=> $faker->buildingNumber
    ];
});
