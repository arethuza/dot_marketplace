<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id_details');

            $table->unsignedBigInteger('id_orders');
            $table->foreign('id_orders')->references('id_orders')->on('orders');

            $table->unsignedBigInteger('id_products');
            $table->foreign('id_products')->references('id')->on('products');

            $table->integer('quantity');
            $table->double('total', 12, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
