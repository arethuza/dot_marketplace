<?php

use App\Products;
use App\Category;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $datas = factory(Products::class, 15)->create([
            'id_kategori'	=> $this->getRandomUserId()
        ]);
    }

    private function getRandomUserId() {
        $user = Category::inRandomOrder()->first();
        return $user->id_kategori;
    }
}
